import {nanoid} from 'nanoid'
import {useState} from 'react'

function App() {


  const [tarea, setTarea]             = useState('')
  const [listaTareas, setListaTareas] = useState([])
  const [modoEdicion, setModoEdicion] = useState(false)
  const [id, setId]                   = useState('')
  const [error, setError]             = useState(null)


//AGREGAR TAREA
  const agregarTarea = (e) => {
    e.preventDefault()

    if(!tarea.trim()) {
      setError('Escriba algo por favor')
      return
    }

    setListaTareas([
      ...listaTareas,
      {
        id: nanoid(5),
        tarea: tarea
      }
    ])

    setTarea('') //esto va a limpiar el form ya que le pasé por parametro que el value va a ser igual a {tarea}
    setError(null)
  }

//BORRAR TAREA
  const borrarTarea = (id) => {

    // console.log(id)
    const arrayFiltrado = listaTareas.filter(item => item.id !== id)
    setListaTareas(arrayFiltrado)

  }


//EDITAR TAREA
  const editar = (item) => {
    // console.log(item)

    setModoEdicion(true)
    setTarea(item.tarea)
    setId(item.id)

    // setModoEdicion(false)
  }

  const editarTarea = (e) => {
      e.preventDefault()

      if(!tarea.trim()) {
        setError('Escriba algo por favor')
        return
      }

      const arrayEditado = listaTareas.map(
        item => item.id === id ? 
        {id, tarea: tarea} 
        : 
        item) //con esto lo que hago es recorrer el array de las tareas y busco la tarea que tiene el mismo id que la que quiero editar y la edito en el input (setTarea) que despues cuando yo apriete el boton voy a definir cual es esa tarea que me la va a devolver al array con su numero de id correspondiente y con la nueva descripcion

      setListaTareas(arrayEditado)
      
      setModoEdicion(false)
      setTarea('')
      setId('')
      setError(null)
  }


  return (
    <div className="container mt-5">
      <h1 className="text-center">CRUD SIMPLE</h1>
      <hr />
      <div className="row">
        <div className="col-8">
          <h4 className="text-center">Lista de tareas</h4>
          <ul className="list-group">
            {
              listaTareas.length === 0 ? 
              (
                <li className="list-group-item">No hay tareas...</li>
              )
              :
              (
              listaTareas.map(item => (
                <li key = {item.id} className="list-group-item">
                  <span className="lead">{item.tarea}</span>
                  <button 
                    onClick={()=>borrarTarea(item.id)}
                    className="btn btn-danger btn-sm float-end mx-2">
                      Eliminar
                  </button>
                  <button 
                    onClick={()=>editar(item)}
                    className="btn btn-warning btn-sm float-end">
                      Editar
                  </button>
                </li>
              ))
              )
            }
          </ul>
        </div>
        <div className="col-4">
          <h4 className="text-center">
            {
              modoEdicion ? 'Editar Tarea' : 'Agregar Tarea'
            }
          </h4>
          <form onSubmit={modoEdicion ? editarTarea : agregarTarea}>
            {
              error ? <span className="text-danger">{error}</span> : null
            }
            <input 
              type="text" 
              className="form-control mb-2"
              placeholder='Ingrese Tarea' 
              onChange={(e) => setTarea(e.target.value)}
              value={tarea} 
            />
            {
              modoEdicion ? 
              (
              <button className="btn btn-warning col-12" type="submit">Editar</button>
              )
              : 
              (
              <button className="btn btn-dark col-12" type="submit">Agregar</button>
              )
            }
          </form>
        </div>
      </div>
    </div>
  );
}

export default App;
